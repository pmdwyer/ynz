#include "Action.hpp"

void ynz::Action::execute( SharedContext* ptrCtx, CharacterStats& source, CharacterStats& target, Spell& spell )
{
    ptrCtx->scriptResource.lua [ "source" ] = std::ref( source );
    ptrCtx->scriptResource.lua [ "target" ] = std::ref( target );
    ptrCtx->scriptResource.lua [ "spell" ] = std::ref( spell );

    auto fullSpellScript = ptrCtx->scenarioConfig.at( "spellScriptDirectory" ).get< std::string >() + spell.spellScript;
    ptrCtx->scriptResource.run( fullSpellScript );
}