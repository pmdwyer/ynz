#include "CharacterAttributes.hpp"

void ynz::CharacterAttributes::from_json( const nlohmann::json& j, CharacterAttributes& characterAttributes )
{
    j.at( "level" ).get_to( characterAttributes.level );
    j.at( "proficiencyBonus" ).get_to( characterAttributes.proficiencyBonus );
    j.at( "hitPoints" ).get_to( characterAttributes.hitPoints );
    j.at( "armorClass" ).get_to( characterAttributes.armorClass );
    j.at( "distance" ).get_to( characterAttributes.distance );
    j.at( "spellSaveDC" ).get_to( characterAttributes.spellSaveDC );
}