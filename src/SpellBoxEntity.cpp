#include "SpellBoxEntity.hpp"

ynz::SpellBoxEntity::SpellBoxEntity( SharedContext *ptrSharedContext )
    : Entity( ptrSharedContext ),
      m_spellBoxShape( sf::Vector2f{ ( float )ptrSharedContext->ptrWindow->getSize().x, 100.0f } )
{
    m_spellBoxShape.setFillColor( sf::Color( 255, 255, 255, 128 ) );
    m_spellBoxShape.setPosition( 0.0f, ptrSharedContext->ptrWindow->getSize().y - 100.f );
}

void ynz::SpellBoxEntity::draw( sf::RenderTarget &target, sf::RenderStates states ) const
{
    target.setView( m_ptrSharedContext->spellView );
    states.transform *= getTransform();
    target.draw( m_spellBoxShape, states );
}