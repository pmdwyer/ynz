#include "SpellSlotEntity.hpp"

ynz::SpellSlotEntity::SpellSlotEntity( SharedContext *ptrSharedContext )
    : Entity( ptrSharedContext ), m_spellBoxShape( sf::Vector2f{ 95.0f, 95.0f } )
{
    m_spellBoxShape.setFillColor( sf::Color( 128, 128, 0, 128 ) );
    m_spellBoxShape.setPosition( 2.5f, ptrSharedContext->ptrWindow->getSize().y - ( 95.f + 2.5f ) );
}

void ynz::SpellSlotEntity::draw( sf::RenderTarget &target, sf::RenderStates states ) const
{
    target.setView( m_ptrSharedContext->spellView );
    states.transform *= getTransform();
    target.draw( m_spellBoxShape, states );
}