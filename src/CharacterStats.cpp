#include "CharacterStats.hpp"

void ynz::CharacterStats::from_json( const nlohmann::json& j, CharacterStats& characterStats )
{
    assert( j.at( "abilities" ).size() == characterStats.abilities.size() );
    assert( j.at( "resistances" ).size() == characterStats.resistances.size() );

    j.at( "name" ).get_to( characterStats.name );

    // teamId is no longer determined by the character sheet but by the scenario
    // config j.at( "teamId" ).get_to( characterStats.teamId );

    // these will be decided later on (maybe -- it might be good to let this be
    // configurable too)
    characterStats.posX = 0;
    characterStats.posY = 0;
    //    j.at( "posX" ).get_to( characterStats.posX );
    //    j.at( "posY" ).get_to( characterStats.posY );

    CharacterAttributes::from_json( j.at( "attributes" ), characterStats.attributes );

    auto abilityElements = j.at( "abilities" );
    auto resistancesElements = j.at( "resistances" );
    auto spellSlotsElements = j.at( "spellSlots" );

    for ( auto i = 0; i < abilityElements.size(); ++i )
        CharacterAbility::from_json( abilityElements.at( i ), characterStats.abilities [ i ] );

    for ( auto i = 0; i < resistancesElements.size(); ++i )
        Damage::from_json( resistancesElements.at( i ), characterStats.resistances [ i ] );

    for ( auto i = 0; i < spellSlotsElements.size(); ++i )
    {
        characterStats.spellSlots.push_back( SpellSlot{} );
        SpellSlot::from_json( spellSlotsElements.at( i ), characterStats.spellSlots [ i ] );
    }
}
