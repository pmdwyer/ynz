#include "MetaMapping.hpp"

#include "CharacterAbility.hpp"
#include "CharacterAttributes.hpp"
#include "CharacterEntity.hpp"
#include "Damage.hpp"

void ynz::from_json( const nlohmann::json& j, Spell& spell ) { Spell::from_json( j, spell ); }

void ynz::from_json( const nlohmann::json& j, CharacterStats& characterStats )
{
    CharacterStats::from_json( j, characterStats );
}

void registerTypes( ynz::ScriptResource& scriptResource )
{
    spdlog::info( "Attempting to register user-defined types for scripting" );

    scriptResource.lua.new_usertype< ynz::Die >( "Die", sol::constructors< ynz::Die( int32_t ) >(), "roll",
                                                 &ynz::Die::roll );

    scriptResource.lua.new_usertype< ynz::Damage >( "Damage", sol::no_constructor, "name", &ynz::Damage::name, "value",
                                                    &ynz::Damage::value );

    scriptResource.lua.new_usertype< ynz::CharacterAttributes >(
        "CharacterAttributes", sol::no_constructor, "level", &ynz::CharacterAttributes::level, "proficiencyBonus",
        &ynz::CharacterAttributes::proficiencyBonus, "hitPoints", &ynz::CharacterAttributes::hitPoints, "armorClass",
        &ynz::CharacterAttributes::armorClass, "distance", &ynz::CharacterAttributes::distance, "spellSaveDC",
        &ynz::CharacterAttributes::spellSaveDC );

    scriptResource.lua.new_usertype< ynz::CharacterAbility >(
        "CharacterAbility", sol::no_constructor, "name", &ynz::CharacterAbility::name, "score",
        &ynz::CharacterAbility::score, "modifier", &ynz::CharacterAbility::modifier, "raceBonus",
        &ynz::CharacterAbility::raceBonus, "abilityImprovement", &ynz::CharacterAbility::abilityImprovement,
        "impairment", &ynz::CharacterAbility::impairment, "points", &ynz::CharacterAbility::points );

    scriptResource.lua.new_usertype< ynz::SpellSlot >( "SpellSlot", sol::no_constructor, "name", &ynz::SpellSlot::name,
                                                       "level", &ynz::SpellSlot::level, "usagesAvailable",
                                                       &ynz::SpellSlot::usagesAvailable );

    scriptResource.lua.new_usertype< ynz::CharacterStats >(
        "CharacterStats", sol::no_constructor, "name", &ynz::CharacterStats::name, "teamId",
        &ynz::CharacterStats::teamId, "abilities", &ynz::CharacterStats::abilities, "attributes",
        &ynz::CharacterStats::attributes, "resistances", &ynz::CharacterStats::resistances, "spellSlots",
        &ynz::CharacterStats::spellSlots, "posX", &ynz::CharacterStats::posX, "posY", &ynz::CharacterStats::posY );

    scriptResource.lua.new_usertype< ynz::Spell >(
      "Spell",                sol::no_constructor,
      "school",               &ynz::Spell::school,
      "name",                 &ynz::Spell::name,
      "level",                &ynz::Spell::level,
      "range",                &ynz::Spell::range,
      "area",                 &ynz::Spell::area,
      "dieSides",             &ynz::Spell::dieSides,
      "actionPoints",         &ynz::Spell::actionPoints,
      "requiresVocalization", &ynz::Spell::requiresVocalization,
      "requiresSomatization", &ynz::Spell::requiresSomatization,
      "damageScale",          &ynz::Spell::damageScale,
      "spellScript",          &ynz::Spell::spellScript
    );

    spdlog::info( "Data types for scripting have been registered" );
}

void loadSpells( const std::string& file, std::unordered_map< std::string, ynz::Spell >& spellMap )
{
    std::ifstream fileStream( file );
    nlohmann::json j;
    fileStream >> j;

    for ( auto i = 0; i < j.at( "spells" ).size(); ++i )
    {
        auto spell = j.at( "spells" ).at( i ).get< ynz::Spell >();
        spellMap.insert( { spell.name, spell } );
    }

    spdlog::info( "Loaded {} spells", spellMap.size() );
}

ynz::CharacterStats loadCharacter( const std::string& file )
{
    std::ifstream fileStream( file );
    nlohmann::json j;
    fileStream >> j;

    return j.get< ynz::CharacterStats >();
}

bool characterHasCorrectSpells( const std::unordered_map< std::string, ynz::Spell >& spellMap,
                                const ynz::CharacterStats& characterStats )
{
    bool isValid = true;

    for ( const auto& spellSlot : characterStats.spellSlots )
    {
        if ( spellMap.find( spellSlot.name ) == spellMap.end() )
        {
            spdlog::error( "Character `{}` has invalid spell `{}`", characterStats.name, spellSlot.name );
            isValid = false;
        }
    }

    return isValid;
}

/***
 * Adds an entity based on a character sheet config file
 * @param ptrCtx the shared context
 * @param file location of json file that contains character info
 * @return true if successful, otherwise false
 */
bool loadCharacterEntity( ynz::SharedContext* ptrCtx, const std::string& file )
{
    // spells must have been loaded
    // entityManager is used directly

    auto character = loadCharacter( file );

    if ( characterHasCorrectSpells( ptrCtx->spellMap, character ) )
    {
        ptrCtx->entityManager.add( ynz::DrawingQueue::MainViewQueue,
                                   std::make_unique< ynz::CharacterEntity >( ptrCtx, character ) );

        return true;
    }

    return false;
}
