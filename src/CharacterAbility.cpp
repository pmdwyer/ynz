#include "CharacterAbility.hpp"

#include "AbilityUtil.hpp"

void ynz::CharacterAbility::from_json( const nlohmann::json& j, CharacterAbility& characterAbility )
{
    j.at( "name" ).get_to( characterAbility.name );
    j.at( "score" ).get_to( characterAbility.score );
    j.at( "modifier" ).get_to( characterAbility.modifier );
    j.at( "raceBonus" ).get_to( characterAbility.raceBonus );
    j.at( "abilityImprovement" ).get_to( characterAbility.abilityImprovement );
    j.at( "impairment" ).get_to( characterAbility.impairment );
    j.at( "points" ).get_to( characterAbility.points );
    characterAbility.abilityType = AbilityUtil::getAbilityTypeId( characterAbility.name );
}