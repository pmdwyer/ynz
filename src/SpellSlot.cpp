#include "SpellSlot.hpp"

void ynz::SpellSlot::from_json( const nlohmann::json& j, SpellSlot& spellSlot )
{
    j.at( "level" ).get_to( spellSlot.level );
    j.at( "name" ).get_to( spellSlot.name );
    j.at( "usagesAvailable" ).get_to( spellSlot.usagesAvailable );
}
