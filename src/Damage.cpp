#include "Damage.hpp"

#include "DamageUtil.hpp"

void ynz::Damage::from_json( const nlohmann::json& j, Damage& damage )
{
    j.at( "name" ).get_to( damage.name );
    j.at( "value" ).get_to( damage.value );
    damage.damageType = DamageUtil::getDamageTypeId( damage.name );
}