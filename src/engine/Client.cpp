#include "engine/Client.hpp"

#include <spdlog/spdlog.h>

#include <SFML/Window/Event.hpp>

#include "Entity.hpp"

ynz::Client::Client( SharedContext *ctx ) : m_sharedCtx{ ctx } { }

void ynz::Client::run()
{
    sf::RenderWindow *ptrWin = m_sharedCtx->ptrWindow;

    float zoomFactor = 1.0f;

    sf::Clock timer;
    const auto timeSlot = 1.0f / 60.0f;

    spdlog::info( "Starting game loop" );

    ptrWin->setFramerateLimit(60);
    while ( ptrWin->isOpen() )
    {
        // poll events
        sf::Event event;
        while ( ptrWin->pollEvent( event ) )
        {
            if ( event.type == sf::Event::Closed )
                ptrWin->close();
            else if ( event.type == sf::Event::MouseWheelScrolled )
            {
                zoomFactor = ( event.mouseWheelScroll.delta < 0 ) ? std::min( 2.0f, zoomFactor + 0.1f )
                                                                  : std::max( 0.5f, zoomFactor - 0.1f );

                m_sharedCtx->mainView.setSize( ( float )ptrWin->getSize().x, ( float )ptrWin->getSize().y );
                m_sharedCtx->mainView.zoom( zoomFactor );
            }
            else
                m_sharedCtx->entityManager.processEvent( event );
        }

        // game logic update
        // TODO: do we need a physics update for calculations?
        if ( timer.getElapsedTime().asSeconds() >= timeSlot )
        {
            m_sharedCtx->entityManager.update( timer.getElapsedTime() );
            timer.restart();
        }

        // draw as fast a possible for now
        ptrWin->clear();
        m_sharedCtx->entityManager.draw( ptrWin );
        ptrWin->display();
    }
}