#include "engine/Server.hpp"

ynz::Server::~Server()
{
    if ( m_thread )
    {
        m_thread->join();
        delete m_thread;
    }
}

void ynz::Server::start()
{
    if ( !m_thread )
    {
        m_thread = new std::thread( &Server::run, this );
    }
}

void ynz::Server::run() { }