#include <spdlog/spdlog.h>

#include <SFML/Graphics.hpp>
#include <algorithm>
#include <cxxopts.hpp>
#include <filesystem>
#include <queue>
#include <unordered_set>
#include <vector>

#include "CharacterEntity.hpp"
#include "engine/Client.hpp"
#include "engine/Server.hpp"
#include "Entity.hpp"
#include "EntityManager.hpp"
#include "FloorEntity.hpp"
#include "MetaMapping.hpp"
#include "SharedContext.hpp"
#include "Spell.hpp"
#include "SpellBoxEntity.hpp"
#include "SpellSlotEntity.hpp"

// the CLI takes a scenario file, which then loads all the configurations
int main( int argc, char *argv [] )
{
    spdlog::set_level( spdlog::level::trace );
    cxxopts::Options options( "murdermage", "a test in Dwyer's abililty to finish anything" );
    options.add_options()( "c,config", "Config file", cxxopts::value< std::string >() );
    auto results = options.parse( argc, argv );
    if ( results.count( "config" ) != 1 )
    {
        spdlog::error( "Config must be specified." );
        spdlog::info( options.help() );
        return EXIT_FAILURE;
    }

    const std::string scenarioConfig = results [ "config" ].as< std::string >();
    if ( !std::filesystem::is_regular_file( scenarioConfig ) )
    {
        spdlog::error( "Could not stat file {}.", scenarioConfig );
        return EXIT_FAILURE;
    }

    std::ifstream scenarioFileStream( scenarioConfig );
    ynz::SharedContext sharedContext;
    scenarioFileStream >> sharedContext.scenarioConfig;

    sf::RenderWindow window(
        sf::VideoMode( sharedContext.scenarioConfig.at( "windowSizeInPixels" ).at( "width" ).get< int32_t >(),
                       sharedContext.scenarioConfig.at( "windowSizeInPixels" ).at( "height" ).get< int32_t >() ),
        "ynz" );

    spdlog::debug("scenario config path: {}", sharedContext.scenarioConfig.at( "spellBookConfigFile" ));

    sharedContext.ptrWindow = &window;
    sharedContext.mainView.reset(
        sf::FloatRect( 0.0f, 0.0f, ( float )window.getSize().x, ( float )window.getSize().y ) );
    sharedContext.spellView.reset(
        sf::FloatRect( 0.0f, 0.0f, ( float )window.getSize().x, ( float )window.getSize().y ) );

    // load the list of all the available spells. this is a superset of all spells, while each character will have a
    // subset of them
    loadSpells( sharedContext.scenarioConfig.at( "spellBookConfigFile" ), sharedContext.spellMap );

    registerTypes( sharedContext.scriptResource );

    // this is the floor, it should come BEFORE the entities on top of it
    sharedContext.entityManager.add( ynz::DrawingQueue::MainViewQueue,
                                     std::make_unique< ynz::FloorEntity >( &sharedContext ) );

    // all the character sheet json file locations
    auto charConfigs = sharedContext.scenarioConfig.at( "characterConfigFiles" );

    // convert the serialized versions to their data types
    // and then add a graphical representation of them
    for ( auto &charConfig : charConfigs )
    {
        auto character = loadCharacter( charConfig.at( "characterConfigFile" ) );
        character.teamId = charConfig.at( "teamId" ).get< int8_t >();

        // TODO: arrange characters based on the teamId
        if ( character.teamId == 1 )
        {
            character.posX = 700;
            character.posY = 700;
        }
        else
        {
            character.posX = 500;
            character.posY = 700;
        }

        sharedContext.entityManager.add( ynz::DrawingQueue::MainViewQueue,
                                         std::make_unique< ynz::CharacterEntity >( &sharedContext, character ) );
    }

    sharedContext.entityManager.add( ynz::DrawingQueue::SpellViewQueue,
                                     std::make_unique< ynz::SpellBoxEntity >( &sharedContext ) );
    sharedContext.entityManager.add( ynz::DrawingQueue::SpellViewQueue,
                                     std::make_unique< ynz::SpellSlotEntity >( &sharedContext ) );

    // create a server and run it in another thread
    ynz::Server server;
    server.start();

    // run the client in the main thread due to opengl context issues
    ynz::Client client( &sharedContext );
    client.run();

    return EXIT_SUCCESS;
}
