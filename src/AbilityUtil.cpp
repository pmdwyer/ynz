#include "AbilityUtil.hpp"

const std::unordered_map< std::string, ynz::AbilityType > ynz::AbilityUtil::abilityNameToAbilityId = {
    { "NONE", AbilityType::NONE }, { "STR", AbilityType::STR }, { "DEX", AbilityType::DEX },
    { "CON", AbilityType::CON },   { "INT", AbilityType::INT }, { "WIS", AbilityType::WIS },
    { "CHA", AbilityType::CHA } };

ynz::AbilityType ynz::AbilityUtil::getAbilityTypeId( const std::string& name )
{
    auto it = abilityNameToAbilityId.find( name );
    assert( it != abilityNameToAbilityId.end() );
    return it->second;
}