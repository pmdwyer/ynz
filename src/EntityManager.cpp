#include "EntityManager.hpp"

#include "Entity.hpp"

void ynz::EntityManager::add( DrawingQueue drawingQueue, std::unique_ptr< Entity >&& ptrEntity )
{
    m_drawingQueues [ drawingQueue ].push_back( std::move( ptrEntity ) );
}

void ynz::EntityManager::processEvent( const sf::Event& event )
{
    for ( const auto& queue : m_drawingQueues )
    {
        for ( const auto& item : queue ) item->processEvent( event );
    }
}

void ynz::EntityManager::update( const sf::Time& delta )
{
    for ( const auto& queue : m_drawingQueues )
    {
        for ( const auto& item : queue ) item->update( delta );
    }
}

void ynz::EntityManager::draw( sf::RenderWindow* ptrWindow ) const
{
    for ( const auto& queue : m_drawingQueues )
    {
        for ( const auto& item : queue ) ptrWindow->draw( *item );
    }
}