#include "Die.hpp"

int32_t ynz::Die::rollD20()
{
    static Die d20( 20 );
    return d20.roll();
}

int32_t ynz::Die::roll( int8_t rolls )
{
    int32_t result = 0;

    while ( rolls > 0 )
    {
        auto amount = m_uniformIntDistribution( m_randomEngine );
        result += amount;
        --rolls;
    }

    return result;
}
