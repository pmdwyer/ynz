#include "CharacterEntity.hpp"

#include <spdlog/spdlog.h>

#include <SFML/Graphics.hpp>

#include "Action.hpp"

ynz::CharacterEntity::CharacterEntity( SharedContext *ptrSharedContext, CharacterStats &characterStats )
    : Entity( ptrSharedContext ), m_player( 15, 20 ), m_playerBody( 15, 20 ), m_isSelected( false ),
      m_characterStats( characterStats )
{
    m_player.setFillColor( sf::Color( 0, 255, 0, 128 ) );
    m_player.setOutlineColor( sf::Color::Green );
    m_player.setOutlineThickness( 2.0f );
    m_player.setOrigin( m_player.getLocalBounds().width / 2.0f, m_player.getLocalBounds().height / 2.0f );
    m_player.scale( 2.0f, 1.0f );
    // m_player.setPosition( 300, 500 );
    m_player.setPosition( ( float )characterStats.posX, ( float )characterStats.posY );

    m_playerBody.setFillColor( sf::Color{ 0, 255, 255, 128 } );
    m_playerBody.setOutlineColor( sf::Color::Cyan );
    m_playerBody.setOutlineThickness( 2.0f );
    m_playerBody.setOrigin( m_playerBody.getLocalBounds().width / 2.0f, m_playerBody.getLocalBounds().height / 2.0f );
    m_playerBody.setScale( 2.0f, 1.0f );
    m_playerBody.setPosition( ( float )characterStats.posX, ( float )characterStats.posY - 50.0f );
}

void ynz::CharacterEntity::draw( sf::RenderTarget &target, sf::RenderStates states ) const
{
    target.setView( m_ptrSharedContext->mainView );
    states.transform *= getTransform();
    target.draw( m_player, states );
    target.draw( m_playerBody, states );
}

void ynz::CharacterEntity::processEvent( const sf::Event &event )
{
    if ( event.type == sf::Event::MouseButtonPressed && event.mouseButton.button == sf::Mouse::Left )
    {
        auto pos =
            m_ptrSharedContext->ptrWindow->mapPixelToCoords( sf::Vector2i( event.mouseButton.x, event.mouseButton.y ) );

        if ( m_isSelected )
        {
            m_playerBody.setPosition( pos.x, pos.y - 50.0f );
            m_player.setPosition( pos );
        }

        auto isSelected = m_player.getGlobalBounds().contains( pos );

        if ( isSelected )
        {
            spdlog::info( "Character selected: `{}`", m_characterStats.name );
            Action::execute( m_ptrSharedContext, m_characterStats, m_characterStats,
                             m_ptrSharedContext->spellMap [ m_characterStats.spellSlots [ 0 ].name ] );
        }
        if ( m_isSelected && isSelected )
            m_isSelected = false;
        else if ( isSelected )
            m_isSelected = isSelected;
    }
}

bool ynz::CharacterEntity::isCharacterSelected() const { return m_isSelected; }

void ynz::CharacterEntity::update( const sf::Time &delta )
{
    if ( m_isSelected )
        m_player.setFillColor( sf::Color( 0, 0, 255, 128 ) );
    else
        m_player.setFillColor( sf::Color( 0, 255, 0, 128 ) );
}
