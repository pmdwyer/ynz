#include "DamageUtil.hpp"

const std::unordered_map< std::string, ynz::DamageType > ynz::DamageUtil::damageNameToDamageType = {
    { "none", DamageType::none },         { "bludgeon", DamageType::bludgeon },   { "piercing", DamageType::piercing },
    { "slashing", DamageType::slashing }, { "lightning", DamageType::lightning }, { "thunder", DamageType::thunder },
    { "poison", DamageType::poison },     { "cold", DamageType::cold },           { "fire", DamageType::fire },
    { "radiant", DamageType::radiant },   { "necrotic", DamageType::necrotic },   { "acid", DamageType::acid },
    { "psychic", DamageType::psychic },   { "force", DamageType::force } };

ynz::DamageType ynz::DamageUtil::getDamageTypeId( const std::string& name )
{
    auto it = damageNameToDamageType.find( name );
    assert( it != damageNameToDamageType.end() );
    return it->second;
}