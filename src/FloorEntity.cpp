#include "FloorEntity.hpp"

ynz::FloorEntity::FloorEntity( ynz::SharedContext *ptrSharedContext )
    : Entity( ptrSharedContext ), m_floorShape( 400, 4 )
{
    m_floorShape.setFillColor( sf::Color( 255, 0, 255, 128 ) );
    m_floorShape.scale( 2.0f, 1.0f );
    m_floorShape.setOrigin( m_floorShape.getLocalBounds().width / 2.0f, m_floorShape.getLocalBounds().height / 2.0f );
    m_floorShape.setPosition( ptrSharedContext->ptrWindow->getSize().x / 2.0f,
                              ptrSharedContext->ptrWindow->getSize().y / 2.0f );
}

void ynz::FloorEntity::draw( sf::RenderTarget &target, sf::RenderStates states ) const
{
    target.setView( m_ptrSharedContext->mainView );
    states.transform *= getTransform();
    target.draw( m_floorShape, states );
}
