
--[[
Basic spell attacks are ones that are:

1. instantaneous
2. only impacts a single turn

There's no need to search for multiple members of a party affected by a spell
because for each potential attack, this script is called:

The followings elements are known:

source
target
spell

die usage:

d = die.new( 20 )

-- roll 1 time
die.roll( 1 )

-- roll 4 times, adding up the result of each roll
die.roll( 4 )

--]]

--d20 = Die.new( 6 )
--print( d20:roll( 4 ) )
print( source.name .. " casts " .. spell.name .. " on target " .. target.name )

