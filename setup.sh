#!/bin/sh

# download dependencies
vcpkg install sfml lua nlohmann-json spdlog sol2 cxxopts

# recursively init submodules
# Note: there will be an issue with building if a submodule shows up on the toolchain path
# the toolchain version will be preferred.
# git submodule update --recursive

# call build
# ./build.sh