#ifndef YNZ_SCRIPTRESOURCE_HPP
#define YNZ_SCRIPTRESOURCE_HPP

#ifndef SOL_ALL_SAFETIES_ON
#define SOL_ALL_SAFETIES_ON 1
#endif

#include <sol/sol.hpp>
#include <unordered_set>

namespace ynz
{
    struct ScriptResource
    {
        sol::state lua;

        ScriptResource() { lua.open_libraries( sol::lib::base, sol::lib::string, sol::lib::io ); }

        bool run( const std::string &file )
        {
            auto result = lua.script_file( file );
            return result.valid();
        }

        // TODO: add one for load, which loads but doesn't run yet
    };
}  // namespace ynz
#endif  // YNZ_SCRIPTRESOURCE_HPP
