#ifndef YNZ_DAMAGEUTIL_HPP
#define YNZ_DAMAGEUTIL_HPP

#include <cassert>
#include <unordered_map>

#include "DamageType.hpp"

namespace ynz
{
    class DamageUtil
    {
      public:
        static DamageType getDamageTypeId( const std::string& name );

      private:
        static const std::unordered_map< std::string, DamageType > damageNameToDamageType;
    };
}  // namespace ynz

#endif  // YNZ_DAMAGEUTIL_HPP