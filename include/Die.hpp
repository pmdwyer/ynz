#ifndef YNZ_DIE_HPP
#define YNZ_DIE_HPP

#include <chrono>
#include <cstdint>
#include <iostream>
#include <random>

namespace ynz
{
    class Die
    {
      public:
        explicit Die( int32_t sides )
            : m_randomEngine( ( unsigned int )std::chrono::system_clock::now().time_since_epoch().count() ),
              m_uniformIntDistribution( 1, sides )
        {
            std::cout << "SIDES: " << sides << std::endl;
        }

        static int32_t rollD20();

        int32_t roll( int8_t rolls = 1 );

        // the default on gcc 7 seems to be linear_congruential_engine
        std::default_random_engine m_randomEngine;
        std::uniform_int_distribution< int32_t > m_uniformIntDistribution;
    };
}  // namespace ynz
#endif  // YNZ_DIE_HPP
