#ifndef YNZ_TURNINFO_HPP
#define YNZ_TURNINFO_HPP

#include "CharacterStats.hpp"
#include "Spell.hpp"

namespace ynz
{
    class TurnInfo
    {
      public:
        TurnInfo() : ptrSpell( nullptr ), ptrSource( nullptr ), ptrTarget( nullptr ), round( 0 ) { }

        void reset()
        {
            ptrSpell = nullptr;
            ptrSource = nullptr;
            ptrTarget = nullptr;
        }

      private:
        Spell* ptrSpell;
        CharacterStats* ptrSource;
        CharacterStats* ptrTarget;
        int32_t round;
    };
}  // namespace ynz

#endif  // YNZ_TURNINFO_HPP