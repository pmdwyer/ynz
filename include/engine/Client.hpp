#ifndef YNZ_CLIENT_HPP
#define YNZ_CLIENT_HPP

#include <thread>

#include "SharedContext.hpp"

namespace ynz
{
    class Client
    {
        // TODO: clean up shared context ownership
      public:
        explicit Client( SharedContext *ctx );
        ~Client() = default;

        void run();

      private:
        SharedContext *m_sharedCtx = nullptr;
    };
}  // namespace ynz

#endif  // YNZ_CLIENT_HPP