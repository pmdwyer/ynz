#ifndef YNZ_SERVER_HPP
#define YNZ_SERVER_HPP

#include <thread>

namespace ynz
{
    class Server
    {
      public:
        Server() = default;
        ~Server();

        void start();

      private:
        std::thread *m_thread = nullptr;

        void run();
    };
}  // namespace ynz

#endif  // YNZ_SERVER_HPP