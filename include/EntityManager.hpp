#ifndef YNZ_ENTITYMANAGER_HPP
#define YNZ_ENTITYMANAGER_HPP

#include <SFML/Graphics/RenderWindow.hpp>
#include <array>
#include <memory>
#include <vector>

namespace ynz
{
    class Entity;

    enum DrawingQueue
    {
        MainViewQueue = 0,
        SpellViewQueue = 1,
        QueueSize
    };

    class EntityManager
    {
      public:
        void add( DrawingQueue drawingQueue, std::unique_ptr< Entity >&& ptrEntity );
        void processEvent( const sf::Event& event );
        void update( const sf::Time& delta );
        void draw( sf::RenderWindow* ptrWindow ) const;

      private:
        std::array< std::vector< std::unique_ptr< Entity > >, DrawingQueue::QueueSize > m_drawingQueues;
    };
}  // namespace ynz

#endif  // YNZ_ENTITYMANAGER_HPP
