#ifndef YNZ_CHARACTERSTATS_HPP
#define YNZ_CHARACTERSTATS_HPP

#include <nlohmann/json.hpp>

#include "CharacterAbility.hpp"
#include "CharacterAttributes.hpp"
#include "Damage.hpp"
#include "SpellSlot.hpp"

namespace ynz
{
    class CharacterStats
    {
      public:
        static void from_json( const nlohmann::json& j, CharacterStats& characterStats );

        std::string name;
        int8_t teamId;
        std::array< CharacterAbility, 6 > abilities;
        CharacterAttributes attributes;

        // also vulnerabilities: -1 to 0 = resistance, 0 - 1 = vulnerability
        // negative because it takes away and is in line with the math
        std::array< Damage, 13 > resistances;

        // the spells available to this character
        std::vector< SpellSlot > spellSlots;

        // easier than using sf::Vector for the time being
        int32_t posX;
        int32_t posY;
    };
}  // namespace ynz

#endif  // YNZ_CHARACTERSTATS_HPP