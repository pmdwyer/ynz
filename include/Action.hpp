#ifndef YNZ_ACTION_HPP
#define YNZ_ACTION_HPP

#include "CharacterStats.hpp"
#include "Entity.hpp"
#include "SharedContext.hpp"
#include "Spell.hpp"

namespace ynz
{
    class Action
    {
      public:
        static void execute( SharedContext* ptrCtx, CharacterStats& source, CharacterStats& target, Spell& spell );
    };

}  // namespace ynz

#endif  // YNZ_ACTION_HPP
