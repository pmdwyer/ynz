#ifndef YNZ_METAMAPPING_HPP
#define YNZ_METAMAPPING_HPP

#include <spdlog/spdlog.h>

#include <cstdint>
#include <fstream>
#include <nlohmann/json.hpp>
#include <unordered_map>

#include "CharacterStats.hpp"
#include "Die.hpp"
#include "ScriptResource.hpp"
#include "SharedContext.hpp"
#include "Spell.hpp"

namespace ynz
{
    void from_json( const nlohmann::json& j, Spell& spell );
    void from_json( const nlohmann::json& j, CharacterStats& characterStats );
}  // namespace ynz

void registerTypes( ynz::ScriptResource& scriptResource );
void loadSpells( const std::string& file, std::unordered_map< std::string, ynz::Spell >& spellMap );
bool characterHasCorrectSpells( const std::unordered_map< std::string, ynz::Spell >& spellMap,
                                const ynz::CharacterStats& characterStats );
ynz::CharacterStats loadCharacter( const std::string& file );
bool loadCharacterEntity( ynz::SharedContext* ptrCtx, const std::string& file );
#endif  // YNZ_METAMAPPING_HPP
