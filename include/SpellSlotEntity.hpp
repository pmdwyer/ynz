#ifndef YNZ_SPELLSLOTENTITY_HPP
#define YNZ_SPELLSLOTENTITY_HPP

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Transformable.hpp>

#include "Entity.hpp"

namespace ynz
{
    class SpellSlotEntity : public Entity, public sf::Transformable
    {
      public:
        explicit SpellSlotEntity( SharedContext *ptrSharedContext );
        ~SpellSlotEntity() { };

        void draw( sf::RenderTarget &target, sf::RenderStates states ) const override;

      private:
        sf::RectangleShape m_spellBoxShape;
    };
}  // namespace ynz

#endif  // YNZ_SPELLSLOTENTITY_HPP