#ifndef YNZ_CHARACTERABILITY_HPP
#define YNZ_CHARACTERABILITY_HPP

#include <nlohmann/json.hpp>
#include <string>

#include "AbilityType.hpp"

namespace ynz
{
    class CharacterAbility
    {
      public:
        static void from_json( const nlohmann::json& j, CharacterAbility& characterAbility );

        std::string name;           // STR, DEX, etc.
        int8_t score;               // the actual value
        int8_t modifier;            // the all-mighty modifier
        int8_t raceBonus;           // used for race-related reasons
        int8_t abilityImprovement;  // used during level-up
        int8_t impairment;          // impairment caused by damage/spell
        int8_t points;              // used during point buy
        AbilityType abilityType;
    };
}  // namespace ynz

#endif  // YNZ_CHARACTERABILITY_HPP