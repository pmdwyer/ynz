#ifndef YNZ_DAMAGETYPE_HPP
#define YNZ_DAMAGETYPE_HPP

namespace ynz
{
    enum class DamageType
    {
        none = 0,
        bludgeon = 1,
        piercing = 2,
        slashing = 3,
        lightning = 4,
        thunder = 5,
        poison = 6,
        cold = 7,
        fire = 8,
        radiant = 9,
        necrotic = 10,
        acid = 11,
        psychic = 12,
        force = 13
    };
}

#endif  // YNZ_DAMAGETYPE_HPP