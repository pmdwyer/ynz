#ifndef YNZ_SHAREDCONTEXT_HPP
#define YNZ_SHAREDCONTEXT_HPP

#include <SFML/Graphics/RenderWindow.hpp>

#include "EntityManager.hpp"
#include "ScriptResource.hpp"
#include "Spell.hpp"

namespace ynz
{
    class SharedContext
    {
      public:
        explicit SharedContext( sf::RenderWindow* ptrRenderWindow ) : ptrWindow( ptrRenderWindow ) { }

        SharedContext() : SharedContext( nullptr ) { }

        sf::RenderWindow* ptrWindow;

        sf::View mainView;
        sf::View spellView;

        ScriptResource scriptResource;
        ynz::EntityManager entityManager;
        std::unordered_map< std::string, ynz::Spell > spellMap;
        nlohmann::json scenarioConfig;
    };
}  // namespace ynz

#endif  // YNZ_SHAREDCONTEXT_HPP
