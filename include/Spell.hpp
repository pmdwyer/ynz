#ifndef YNZ_SPELL_HPP
#define YNZ_SPELL_HPP

#include <cstdint>
#include <cstring>
#include <nlohmann/json.hpp>
#include <tuple>
#include <vector>

namespace ynz
{
    struct Spell
    {
        std::string school;
        std::string name;

        int8_t level;
        int8_t range;
        int8_t area;

        int8_t dieSides;
        int8_t actionPoints;

        bool requiresVocalization;
        bool requiresSomatization;

        // levelStart, dieRolls
        std::vector< std::tuple< int8_t, int8_t > > damageScale;

        std::string spellScript;

        static void from_json( const nlohmann::json& j, Spell& spell )
        {
            j.at( "school" ).get_to( spell.school );
            j.at( "name" ).get_to( spell.name );
            j.at( "level" ).get_to( spell.level );
            j.at( "range" ).get_to( spell.range );
            j.at( "area" ).get_to( spell.area );
            j.at( "dieSides" ).get_to( spell.dieSides );
            j.at( "actionPoints" ).get_to( spell.actionPoints );
            j.at( "requiresVocalization" ).get_to( spell.requiresVocalization );
            j.at( "requiresSomatization" ).get_to( spell.requiresSomatization );
            j.at( "spellScript" ).get_to( spell.spellScript );

            auto damageScaleElement = j.at( "damageScale" );

            for ( auto& i : damageScaleElement )
            {
                spell.damageScale.emplace_back( i.at( "levelStart" ).get< int8_t >(),
                                                i.at( "dieRolls" ).get< int8_t >() );
            }
        }
    };

}  // namespace ynz

#endif  // YNZ_SPELL_HPP
