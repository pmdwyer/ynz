#ifndef YNZ_ENTITIES_HPP
#define YNZ_ENTITIES_HPP

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/Window/Event.hpp>

#include "SharedContext.hpp"

namespace ynz
{
    class Entity : public sf::Drawable
    {
      public:
        Entity() : m_isAlive( false ) { }

        Entity( SharedContext *ctx ) : m_isAlive( false ), m_ptrSharedContext( ctx ) { }

        virtual ~Entity() = default;

        // these can be overridden in order to be tied into the event system
        virtual void processEvent( const sf::Event &event ) { }

        virtual void update( const sf::Time &delta ) { }

        virtual bool isAlive() const { return m_isAlive; }

        virtual void kill() { m_isAlive = false; }

      protected:
        bool m_isAlive;
        SharedContext *m_ptrSharedContext;
    };
}  // namespace ynz
#endif  // YNZ_ENTITIES_HPP
