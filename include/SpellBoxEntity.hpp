#ifndef YNZ_SPELLBOXENTITY_HPP
#define YNZ_SPELLBOXENTITY_HPP

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Transformable.hpp>

#include "Entity.hpp"

namespace ynz
{
    class SpellBoxEntity : public Entity, public sf::Transformable
    {
      public:
        explicit SpellBoxEntity( SharedContext *ptrSharedContext );
        ~SpellBoxEntity() { };

        void draw( sf::RenderTarget &target, sf::RenderStates states ) const override;

      private:
        sf::RectangleShape m_spellBoxShape;
    };
}  // namespace ynz

#endif  // YNZ_SPELLBOXENTITY_HPP