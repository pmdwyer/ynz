#ifndef YNZ_DAMAGE_HPP
#define YNZ_DAMAGE_HPP

#include <nlohmann/json.hpp>
#include <string>

#include "DamageType.hpp"

namespace ynz
{
    class Damage
    {
      public:
        static void from_json( const nlohmann::json& j, Damage& damage );

        std::string name;
        float value;
        DamageType damageType;
    };
}  // namespace ynz

#endif  // YNZ_DAMAGE_HPP