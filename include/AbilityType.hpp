#ifndef YNZ_ABILITYTYPE_HPP
#define YNZ_ABILITYTYPE_HPP

namespace ynz
{
    enum class AbilityType
    {
        NONE = -1,
        STR = 0,
        DEX = 1,
        CON = 2,
        INT = 3,
        WIS = 4,
        CHA = 5
    };
}

#endif  // YNZ_ABILITYTYPE_HPP