#ifndef YNZ_FLOORENTITY_HPP
#define YNZ_FLOORENTITY_HPP

#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/Transformable.hpp>

#include "Entity.hpp"

namespace ynz
{
    class FloorEntity : public Entity, public sf::Transformable
    {
      public:
        explicit FloorEntity( SharedContext *ptrSharedContext );
        ~FloorEntity() { };

        void draw( sf::RenderTarget &target, sf::RenderStates states ) const override;

      private:
        sf::CircleShape m_floorShape;
    };
}  // namespace ynz

#endif  // YNZ_FLOORENTITY_HPP