#ifndef YNZ_SPELLSLOT_HPP
#define YNZ_SPELLSLOT_HPP

#include <nlohmann/json.hpp>
#include <string>

namespace ynz
{
    class SpellSlot
    {
      public:
        static void from_json( const nlohmann::json& j, SpellSlot& spellSlot );

        // this name is used as a lookup in the spellbook/spellMap so they must match
        // up in order to work
        std::string name;
        int8_t level;
        int8_t usagesAvailable;
    };
}  // namespace ynz

#endif  // YNZ_SPELLSLOT_HPP