#ifndef YNZ_CHARACTERATTRIBUTES_HPP
#define YNZ_CHARACTERATTRIBUTES_HPP

#include <cstdint>
#include <nlohmann/json.hpp>

namespace ynz
{
    class CharacterAttributes
    {
      public:
        static void from_json( const nlohmann::json& j, CharacterAttributes& characterAttributes );

        int8_t level;
        int8_t proficiencyBonus;
        int8_t hitPoints;
        int8_t armorClass;
        int8_t distance;
        int8_t spellSaveDC;
    };
}  // namespace ynz

#endif  // YNZ_CHARACTERATTRIBUTES_HPP