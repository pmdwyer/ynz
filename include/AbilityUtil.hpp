#ifndef YNZ_ABILITYUTIL_HPP
#define YNZ_ABILITYUTIL_HPP

#include <cassert>
#include <string>
#include <unordered_map>

#include "AbilityType.hpp"

namespace ynz
{
    struct AbilityUtil
    {
      public:
        static AbilityType getAbilityTypeId( const std::string& name );

      private:
        static const std::unordered_map< std::string, AbilityType > abilityNameToAbilityId;
    };
}  // namespace ynz
#endif  // YNZ_ABILITYUTIL_HPP
