#ifndef YNZ_CHARACTERENTITY_HPP
#define YNZ_CHARACTERENTITY_HPP

#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/Transformable.hpp>

#include "CharacterStats.hpp"
#include "Entity.hpp"

namespace ynz
{
    class CharacterEntity : public Entity, public sf::Transformable
    {
      public:
        explicit CharacterEntity( SharedContext *ptrSharedContext, CharacterStats &characterStats );
        ~CharacterEntity() { };

        void draw( sf::RenderTarget &target, sf::RenderStates states ) const override;

        void processEvent( const sf::Event &event ) override;

        bool isCharacterSelected() const;

        void update( const sf::Time &delta ) override;

        CharacterStats &getStats() { return m_characterStats; }

      private:
        sf::CircleShape m_player;
        sf::CircleShape m_playerBody;

        bool m_isSelected;

        CharacterStats m_characterStats;
    };
}  // namespace ynz

#endif  // YNZ_CHARACTERENTITY_HPP