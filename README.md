# Welcome, Yinz

## Setup
Simply clone the repo and run setup.sh in the main directory. You will need to have vcpkg installed and on the path.

`./setup.sh`

### Required libraries
* sfml
* lua
* sol2
* nlohmann json
* spdlog
* cxxopts

## Build
From the root directory run

`./build.sh`

## Formatting
From the root directory run

`find . \( -path ./thirdparty -o -path ./build \) -prune -false -o -type f -iname \*.hpp -o -iname \*.cpp | xargs -L 1 clang-format -style=file -i`

## TODO
* test setup
* add formatting pre-push git hook
* make config paths relative
* EntityManager contain Entity data arrays
* Entity only have handle to EM index
* RenderSystem
  * Layers with EIDs on each Layer
* Better input handling
* Server / Client threads with actions