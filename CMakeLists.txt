cmake_minimum_required( VERSION 3.10 )
project( murdermage )

####################################################################################################
#
# Lua & Sol
# 1.  download lua (do NOT use package manager), grabbing the tar.gz file
# 2.  untar and run: make linux test
# 3.  everything will be in the src dir
# 4.  link libdl and lua
#
####################################################################################################
include( FindLua )
find_package( Lua 5.3 REQUIRED )
find_package( SFML 2.5 COMPONENTS system audio graphics window network REQUIRED )
find_package( nlohmann_json 3.8 CONFIG REQUIRED )
find_package( sol2 3.2 CONFIG REQUIRED )
find_package( spdlog 1.6 CONFIG REQUIRED )
find_package( cxxopts 2.2 CONFIG REQUIRED )

# back up imconfig.h for imgui (this is probably unnessesary)
# if (EXISTS ${CMAKE_SOURCE_DIR}/thirdparty/imgui/imconfig.h AND NOT EXISTS ${CMAKE_SOURCE_DIR}/thirdparty/imgui/imconfig.h.bak)
#   file(RENAME ${CMAKE_SOURCE_DIR}/thirdparty/imgui/imconfig.h ${CMAKE_SOURCE_DIR}/thirdparty/imgui/imconfig.h.bak)
# endif ()
# configure imgui to enable use with sfml
# file(COPY ${CMAKE_SOURCE_DIR}/thirdparty/imgui-sfml/imconfig-SFML.h DESTINATION ${CMAKE_SOURCE_DIR}/thirdparty/imgui)
# file(RENAME ${CMAKE_SOURCE_DIR}/thirdparty/imgui/imconfig-SFML.h ${CMAKE_SOURCE_DIR}/thirdparty/imgui/imconfig.h)

add_executable( ${PROJECT_NAME} src/main.cpp )

# add source files
target_sources( ${PROJECT_NAME} 
  PRIVATE
    src/AbilityUtil.cpp
    src/Action.cpp
    src/CharacterAbility.cpp
    src/CharacterAttributes.cpp
    src/CharacterEntity.cpp
    src/CharacterStats.cpp
    src/Damage.cpp
    src/DamageUtil.cpp
    src/Die.cpp

    src/engine/Client.cpp
    src/engine/Server.cpp

    src/EntityManager.cpp
    src/FloorEntity.cpp
    src/MetaMapping.cpp
    src/SpellBoxEntity.cpp
    src/SpellSlot.cpp
    src/SpellSlotEntity.cpp
)

# add include dirs
target_include_directories( ${PROJECT_NAME}
  PRIVATE
    include
    sfml-graphics
    sfml-window
    sfml-network
    sfml-audio
    sfml-system
    opengl32
    ${LUA_INCLUDE_DIR}
)

# add lib dirs
target_link_libraries( ${PROJECT_NAME}
  PRIVATE
    sfml-graphics
    sfml-window
    sfml-network
    sfml-audio
    sfml-system
    GL
    X11
    FLAC
    UDev
    OpenAL
    ${LUA_LIBRARIES}
    nlohmann_json::nlohmann_json
    cxxopts::cxxopts
    sol2::sol2
    spdlog::spdlog
)

target_compile_features( ${PROJECT_NAME}
  PRIVATE
    cxx_std_17
)